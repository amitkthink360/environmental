import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.less']
})
export class ProductComponent implements OnInit {

  constructor() { }
	 public items = [
	  {
		 "id":"1",
		 "image":"./assets/img/Tile_Image.jpg",
		 "price":"$7.50/ton",
		 "location":'Kariba Redd+ -Zimbabwe',
		 "title":"Reduced Emissions from Deforestation and Degradation",
		 "weight":"957,842 lbs. offset",
		 "description":"Protects forests and wildlife on the southern shores of Lake Kariba in Zimbabwe, forming a giant biodiversity corridor."
	  },
	  {
		 "id":"1",
		 "image":"./assets/img/Tile_Image.jpg",
		 "price":"",
		 "location":'Terrapass Mixed Portfolio - Mixed',
		 "title":"Mixed",
		 "weight":"241,967 lbs. offset",
		 "description":"Grenhouse gas reductions made at a variety of projects, including: reforestation, methane capture and renewable energy."
	  },
	  {
		 "id":"1",
		 "image":"./assets/img/Tile_Image.jpg",
		 "price":"$7.50/ton",
		 "location":'Kariba Redd+ -Zimbabwe',
		 "title":"Reduced Emissions from Deforestation and Degradation",
		 "weight":"957,842 lbs. offset",
		 "description":"Protects forests and wildlife on the southern shores of Lake Kariba in Zimbabwe, forming a giant biodiversity corridor."
	  },
	  {
		 "id":"1",
		 "image":"./assets/img/Tile_Image.jpg",
		 "price":"",
		 "location":'Terrapass Mixed Portfolio - Mixed',
		 "title":"Mixed",
		 "weight":"241,967 lbs. offset",
		 "description":"Grenhouse gas reductions made at a variety of projects, including: reforestation, methane capture and renewable energy."
	  },
	  {
		 "id":"1",
		 "image":"./assets/img/Tile_Image.jpg",
		 "price":"$7.50/ton",
		 "location":'Kariba Redd+ -Zimbabwe',
		 "title":"Reduced Emissions from Deforestation and Degradation",
		 "weight":"957,842 lbs. offset",
		 "description":"Protects forests and wildlife on the southern shores of Lake Kariba in Zimbabwe, forming a giant biodiversity corridor."
	  },
	  {
		 "id":"1",
		 "image":"./assets/img/Tile_Image.jpg",
		 "price":"",
		 "location":'Terrapass Mixed Portfolio - Mixed',
		 "title":"Mixed",
		 "weight":"241,967 lbs. offset",
		 "description":"Grenhouse gas reductions made at a variety of projects, including: reforestation, methane capture and renewable energy."
	  }
	];
  ngOnInit() {
  }

}
